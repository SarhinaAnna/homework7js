"use strict";

function filterBy(array, typedata) {
  return array.filter((newarray) => typeof newarray !== typedata);
}
console.log(filterBy(["hello", "world", 23, "23", null], "string"));
